#include <stdio.h>
#define MAXLINE 419001 /* maximum input line size */

int get_line(char line[], int maxline);

/* print longest input line */
int main() {
    int len;                /* current line length      */
    int min;                /* minimum line length      */
    char line[MAXLINE];     /* current input line       */

    min = 80;
    while ((len = get_line(line, MAXLINE)) > 0) {
        if (len > min) {
            printf("%s\n", line);
        }
    }
    return 0;
}

/* getline: read a line into s, return length */
int get_line(char s[], int lim) {
    int c, i;

    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }
   
    if (c == '\n') {
        s[i] = c;
        ++i;
    }

    s[i] = '\0';

    return i;
}
