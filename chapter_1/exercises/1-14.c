#include <stdio.h>

void horizontal() {
    int c;
    int lengths[26];

    for (int i = 0; i < 26; i++){
        lengths[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        if (c >= 'a' && c <= 'z') {
            ++lengths[c - 'a'];
        }
    }

    printf("----------------------------------\n");
    for (int i = 0; i < 26; i++) {
        if (lengths[i] > 0) {
            printf("%c: ", 'a' + i);
            for (int j = 0; j < lengths[i]; j++) {
                printf("#");
            }
        printf("\n");
        }
    }
    printf("----------------------------------\n");
}

void vertical() {
    int c;
    int max_length = 0;
    int lengths[26];

    for (int i = 0; i < 26; i++) {
        lengths[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        if (c >= 'a' && c <= 'z') {
            ++lengths[c - 'a'];
            if (lengths[c - 'a'] >= max_length) {
                max_length = lengths[c - 'a'];
            }
        }               
    }

    printf("----------------------------------\n");
    for (int i = 0; i < 26; i++) {
        if (lengths[i] > 0) {
            printf("%c", 'a' + i);
        }
    }
    printf("\n");
    for (int i = max_length; i > 0; i--) {
        for (int j = 0; j < 26; j++) {
            if (lengths[j] == i) {
                printf("#");
                lengths[j]--;
            } else if (lengths[j] > 0){
                printf(" ");
            }
        }
        printf("\n");
    }
    printf("----------------------------------\n");
}

int main() {
    int option;
    printf("Enter 1 to print the horizontal histogram or");
    printf(" 2 to print the vertical histogram\n");

    scanf("%d", &option);
    getchar();

    if (option == 1) {
        horizontal();
    } else if (option == 2) {
        vertical();
    } else {
        printf("Incorrect option\n");
    }

    return 0;
}
