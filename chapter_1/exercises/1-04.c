#include <stdio.h>

/* printf Celcius-Fahrenheit table
    for celcius = 0, 20, ..., 300; floating-point version */

int main() {
    float fahr, celcius;
    int lower, upper, step;

    lower = 0;      /* lower limit of temperature table */
    upper = 300;    /* upper limit */
    step = 20;      /* step size */

    celcius = lower;

    printf("CELCIUS TO FAHRENHEIT TABLE\n");
    printf("---------------------------\n");
    printf("CELCIUS \t FAHRENHEIT\n");
    printf("---------- \t -------\n");

    while (celcius <= upper) {
        fahr = celcius * (9.0 / 5.0) + 32.0;
        printf("%6.0f \t\t %5.0f\n", celcius, fahr);
        celcius = celcius + step;
    }
    return 0;
}
