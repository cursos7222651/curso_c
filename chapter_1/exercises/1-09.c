#include <stdio.h>

/* 
 * copy input to output replacing each string of one or more blank by a single
 * blank
 */

int main() {
    int c;
    int blank = 0;

    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            blank = 1; 
        } else {
            if (blank) {
                putchar(' ');
            }
            putchar(c);
            blank = 0;
        }
    }

    return 0;
}
