#include <stdio.h>

/* print Fahrenheit-Celcius table
    for fahr = 0, 20, ..., 300 */

float convert(float fahr);

int main() {
    float fahr;
    int lower, upper, step;

    lower = 0;      /* lower limit of temperature table */
    upper = 300;    /* upper limit */
    step = 20;      /* step size */

    fahr = lower;

    printf("FAHRENHEIT TO CELCIUS TABLE\n");
    printf("---------------------------\n");
    printf("FAHRENHEIT \t CELCIUS\n");
    printf("---------- \t --------\n");

    while (fahr <= upper) {
        printf("%5.0f \t\t %6.1f\n", fahr, convert(fahr));
        fahr = fahr + step;
    }

    return 0;
}

// returns the celcius equivalent to the fahrenheit temperature
float convert(float fahr) {
    return (5.0 / 9.0) * (fahr - 32.0);
}
