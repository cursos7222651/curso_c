#include <stdio.h>

int main() {
    int c;

    if ((c = getchar()) == EOF) {
        printf("EOF is equal to 1\n");
    } else {
        printf("EOF is equal to 0\n");
    }

    return 0;
}
