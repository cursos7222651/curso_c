#include <stdio.h>
#include <stdlib.h>

void horizontal() {
    int c;
    int* lengths = malloc(sizeof(int)); // store the length of each word
    lengths[0] = 0;
    int number_words = 0;

    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\t' || c == '\n') {
            // end of word
            ++number_words;
            int* temp = lengths;
            lengths = malloc(number_words * sizeof(int));
            lengths = temp;
            lengths[number_words] = 0;
        } else {
            ++lengths[number_words];
        }
    }

    printf("----------------------------------\n");
    for (int i = 0; i < number_words; i++) {
        for (int j = 0; j < lengths[i]; j++) {
            printf("#");
        }
        printf("\n");
    }
    printf("----------------------------------\n");
    free(lengths);
}

void vertical() {
    int c;
    int* lengths = malloc(sizeof(int));
    int number_words = 0;
    int max_length = 0;
    lengths[0] = 0;

    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\t' || c == '\n') {
            if (lengths[number_words] >= max_length) {
                max_length = lengths[number_words];
            }

            ++number_words;
            int* temp = lengths;
            lengths = malloc(number_words * sizeof(int));
            lengths = temp;
            lengths[number_words] = 0;
        } else {
            ++lengths[number_words];
        }
    }

    printf("----------------------------------\n");
    for (int i = max_length; i > 0; i--) {
        for (int j = 0; j < number_words; j++) {
            if (lengths[j] == i) {
                printf("#");
                lengths[j]--;
            } else {
                printf(" ");
            }
        }
        printf("\n");
    }
    printf("----------------------------------\n");
    free(lengths);
}

int main() {
    int option;
    printf("Enter 1 to print the horizontal histogram or");
    printf(" 2 to print the vertical histogram\n");

    scanf("%d", &option);
    getchar();

    if (option == 1) {
        horizontal();
    } else if (option == 2) {
        vertical();
    } else {
        printf("Incorrect option\n");
    }

    return 0;
}
