#include <stdio.h>

#define MAX_LENGTH 1000

void reverse(char s[MAX_LENGTH], char r[MAX_LENGTH]);

int main() {
    char s[MAX_LENGTH];
    char r[MAX_LENGTH];

    s[0] = '\0';
    scanf("%[^\n]", s);

    reverse(s, r);
    printf("Original string is: %s\nReverse string is: %s\n", s, r);

    return 0;
}

// reverse a string of size MAX_LENGTH
void reverse(char s[MAX_LENGTH], char r[MAX_LENGTH]) {
    int len = 0;
    int j = 0;

    for (int i = 0; i < MAX_LENGTH; i++) {
        if (s[i] == '\0') {
            len = i;
            break;
        }
    } 

    for (int i = len - 1; i >= 0; i--) {
        r[j] = s[i];
        j++;
    }

    r[j] = '\0';
}
