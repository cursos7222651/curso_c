#include <stdio.h>
#include <string.h>
#define MAXLINE 4190014 /* maximum input line size */

void get_line(char line[], int maxline);

/* print longest input line */
int main() {
    char line[MAXLINE];     /* current input line         */

    get_line(line, MAXLINE);
    printf("%s\n", line);
   
    return 0;
}

/* getline: read a line into s, return length */
void get_line(char s[], int lim) {
    int c, i;

    for (i = 0; i < lim - 1 && (c = getchar()) != EOF; ++i) {
        if (c != '\n' && c != '\t' && c != ' ') {
            s[i] = c;
        } else {
            i--;
        }
    }
    s[i] = '\0';
}
